#!/usr/bin/python

""" shengjing  """
""" version 0.1 """
""" date 2018-11-27 """
""" server end to parse ftp received file to DB"""



import socket
import datetime
import time
import csv
import pymysql
import os
import shutil

from ftplib import FTP


""" check file folder, found the received file """

def locate_new_received_file(path):

    return os.path.isfile(path)

"""DATA file to Remote server"""

# insert directly to remote DB
def store_2_remote_db(file_dir):
    conn = pymysql.connect(host = "138.100.82.177", port = 3306, user = "root", passwd = "owntracks", db = "mushroom")
    cur = conn.cursor()
    row_count = 100
    
    query_insert_str = "insert into mushroom_measurement (location, timestamp, pm2_5, pm10, tvoc, co2, temperature, humidity, illumination, noise, hcho, co,c6h6, no2, o3) VALUES "
    query_value_str = ""
    
    # every 500 rows, insert once to DB
    input_file = csv.DictReader(open(file_dir))
    for row in input_file:
        if row_count > 0:
            query_value_str = query_value_str + "(" + "'" + row["location"] +  "'" + "," + "'"  + row["timestamp"] + "'" + "," + row["pm2_5"] + "," +  row["pm10"] + "," +  row["tvoc"] + "," + row["co2"] + "," + row["temperature"] + "," + row["humidity"] + "," + row["illumination"] + "," + row["noise"] + "," + row["hcho"]+ "," + row["co"] + "," + row["c6h6"] + "," + row["no2"] + "," + row["o3"]+ ")" + ","
            row_count = row_count - 1
        else:
            query_value_str = ""
            row_count = 100
            query_value_str = query_value_str[0: len(query_value_str) -1]
            query_insert_statement = query_insert_str + query_value_str
            print (query_insert_statement)
            cur.execute(query_insert_statement)
            conn.commit()
    
    query_value_str = query_value_str[0: len(query_value_str) -1 ]
    query_insert_statement = query_insert_str + query_value_str
    print (query_insert_statement)
    cur.execute(query_insert_statement)
    conn.commit()

    cur.close()
    conn.close()
        

def clear_file(path):
    if os.path.isfile(path):
        os.remove(path)
    else:
        print ('No file found !')

""" Entrance"""

file_ftp_receive_path = '/home/sun/file_received.csv'

while True:

    # checking new received file
    print ("checking new received files")
    file_exist = locate_new_received_file(file_ftp_receive_path)
    if file_exist:
        print ("Found a received file")
        ts = time.time()
        print ('Backup the received file to backup folder')
        dt = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M")
        shutil.copy2(file_ftp_receive_path, '/home/sun/IAQ_SUN/FTP/data_ftp_receiving_backup/file_received_' + dt + '.csv')
        print ("Store to DB")
        try:
            store_2_remote_db(file_ftp_receive_path)
        except:
            print ('DB is down !')
        finally:
            # delete the new received file
            print ("delete the file")
            clear_file(file_ftp_receive_path)
    # wait for 5 mins
    else:
        print ('Not found file !')
    time.sleep(30)






