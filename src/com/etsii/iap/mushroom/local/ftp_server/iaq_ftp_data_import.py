#!/usr/bin/python

""" shengjing  """
""" version 0.1 """
""" date 2018-11-27 """
""" manually import ftp files to DB"""



import datetime
import time
import csv
import pymysql
import os
import shutil
from pathlib import Path



# insert directly to remote DB
def store_2_remote_db(file_dir):
    conn = pymysql.connect(host = "138.100.82.177", port = 3306, user = "root", passwd = "owntracks", db = "mushroom")
    cur = conn.cursor()
    row_count = 100
    
    query_insert_str = "insert into mushroom_measurement (location, timestamp, pm2_5, pm10, tvoc, co2, temperature, humidity, illumination, noise, hcho, co,c6h6, no2, o3) VALUES "
    query_value_str = ""
    
    # every 500 rows, insert once to DB
    input_file = csv.DictReader(open(file_dir))
    for row in input_file:
        if row_count > 0:
            query_value_str = query_value_str + "(" + "'" + row["location"] +  "'" + "," + "'"  + row["timestamp"] + "'" + "," + row["pm2_5"] + "," +  row["pm10"] + "," +  row["tvoc"] + "," + row["co2"] + "," + row["temperature"] + "," + row["humidity"] + "," + row["illumination"] + "," + row["noise"] + "," + row["hcho"]+ "," + row["co"] + "," + row["c6h6"] + "," + row["no2"] + "," + row["o3"]+ ")" + ","
            row_count = row_count - 1
        else:
            query_value_str = ""
            row_count = 100
            query_value_str = query_value_str[0: len(query_value_str) -1]
            query_insert_statement = query_insert_str + query_value_str
            print (query_insert_statement)
            cur.execute(query_insert_statement)
            conn.commit()
    
    query_value_str = query_value_str[0: len(query_value_str) -1 ]
    query_insert_statement = query_insert_str + query_value_str
    print (query_insert_statement)
    cur.execute(query_insert_statement)
    conn.commit()

    cur.close()
    conn.close()
        

def del_file(f):
    
    if os.path.isfile(f):
        os.remove(f)
    else:
        print ('No file found !')

""" Entrance"""

for f in os.listdir('/home/sun/IAQ_SUN/FTP/data_ftp_receiving_backup/'):
    print (f)
    store_2_remote_db('/home/sun/IAQ_SUN/FTP/data_ftp_receiving_backup/' + f)
    # delete file
    print ("delete the file" + f)
    del_file('/home/sun/IAQ_SUN/FTP/data_ftp_receiving_backup/' + f)






