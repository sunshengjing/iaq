#!/usr/bin/python

""" shengjing  """
""" version 0.1 """
""" date 2018-10-11 """
""" data collection phase"""



import socket
import datetime
import time
import csv
import pymysql

from ftplib import FTP

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from email.utils import COMMASPACE


""" create a new file
    store data values """

def create_new_file(file_basic_name, file_count):
	f = open (file_basic_name + str(file_count) + ".csv", "w" )
	return f


"""parse hex value to decimal"""
def hex2decimal (hex_value_str, hex_decimal_displacement):
	decimal_displacement = 0
	if int(hex_decimal_displacement, 16) == 0:
		decimal_displacement = 1
	elif int(hex_decimal_displacement, 16) == 1:
		decimal_displacement = 10
	elif int(hex_decimal_displacement, 16) ==2:
		decimal_displacement = 100
	elif int(hex_decimal_displacement, 16) == 3:
		decimal_displacement = 1000
	elif int(hex_decimal_displacement, 16) == 4:
		decimal_displacement = 10000
	elif int (hex_decimal_displacement, 16) == 5:
		decimal_displacement = 100000
	else:
		pass
	return int(hex_value_str, 16) / float (decimal_displacement)


"""average the values in 1 minute"""

def avg_data_1_min(measuremnt_1_min_list):
	if len(measuremnt_1_min_list) != 0:
	    return round((sum(measuremnt_1_min_list) / len(measuremnt_1_min_list)), 3)
	else: 
		return 0

"""parse data measuremnt in data_1_min_list
# 50 32 PM2.5
# 50 41 PM10
# 54 56 TVOC
# 43 32 CO2
# 54 50 TP
# 48 52 HR
# 4C 4D LM
# 4E 49 NI
# 48 4F HCHO
# 43 31 CO
# 67 54 C6H6
# 78 50 N02
# 79 51 O3
"""

def parse_data_1_min_list(data_1_min_list):
    data_measurment_dict = {}
    data_measurment_dict["pm2_5"] = []
    data_measurment_dict["pm10"] = []
    data_measurment_dict["tvoc"] = []
    data_measurment_dict["co2"] = []
    data_measurment_dict["temperature"] = []
    data_measurment_dict["humidity"] = []
    data_measurment_dict["illumination"] = []
    data_measurment_dict["noise"] = []
    data_measurment_dict["hcho"] = []
    data_measurment_dict["co"] = []
    data_measurment_dict["c6h6"] = []
    data_measurment_dict["no2"] = []
    data_measurment_dict["o3"] = []
    for hex_str in data_1_min_list:
        sensor_code = ""
        sensor_code = hex_str[50:54]
        #print (sensor_code)
        sensor_value = ""
        sensor_value = hex_str[44:48]
        sensor_decimal_displacement = ""
        sensor_decimal_displacement = hex_str[42:44]
        if sensor_code == "5032":
            data_measurment_dict["pm2_5"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "5041":
            data_measurment_dict["pm10"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "5456":
            data_measurment_dict["tvoc"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "4332":
            data_measurment_dict["co2"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "5450":
            data_measurment_dict["temperature"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "4852":
            data_measurment_dict["humidity"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "4C4D":
            data_measurment_dict["illumination"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "4E49":
            data_measurment_dict["noise"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "484F":
            data_measurment_dict["hcho"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif sensor_code == "4331":
            data_measurment_dict["co"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "4336":
            data_measurment_dict["c6h6"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code== "4e32":
            data_measurment_dict["no2"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        elif  sensor_code == "4F33":
            data_measurment_dict["o3"].append(hex2decimal(sensor_value, sensor_decimal_displacement))
        else:
            pass
        #print ("tvoc list")
        #print (data_measurment_dict["tvoc"])

    return {"pm2_5":avg_data_1_min(data_measurment_dict["pm2_5"]),
            "pm10":avg_data_1_min(data_measurment_dict["pm10"]),
            "tvoc":avg_data_1_min(data_measurment_dict["tvoc"]),
                "co2":avg_data_1_min(data_measurment_dict["co2"]),
                "temperature":avg_data_1_min(data_measurment_dict["temperature"]),
                "humidity":avg_data_1_min(data_measurment_dict["humidity"]),
                "illumination":avg_data_1_min(data_measurment_dict["illumination"]),
                "noise":avg_data_1_min(data_measurment_dict["noise"]),
                "hcho":avg_data_1_min(data_measurment_dict["hcho"]),
                "co":avg_data_1_min(data_measurment_dict["co"]),
                "c6h6":avg_data_1_min(data_measurment_dict["c6h6"]),
                "no2":avg_data_1_min(data_measurment_dict["no2"]),
                "o3":avg_data_1_min(data_measurment_dict["o3"])}


""" store all measuremetns of a minute in a stirng list"""

def data_1_min_collect(seconds, sock):
    start = time.time()  
    elapsed = 0
    data_1_min_list = []
    while elapsed < seconds:
        data,addr = sock.recvfrom(1024)
        hex_str = "".join(["{:02X}".format(ord(c)) for c in data])
        data_1_min_list.append(hex_str)
        time.sleep(1)
        elapsed = time.time() - start
        
    #print data_1_min_list
    return data_1_min_list  


"""DATA file to Remote server"""

# insert directly to remote DB
def store_2_remote_db(file_name):
    conn = pymysql.connect(host = "138.100.82.177", port = 3306, user = "root", passwd = "owntracks", db = "mushroom")
    cur = conn.cursor()
    row_count = 100
    
    query_insert_str = "insert into mushroom_measurement (location, timestamp, pm2_5, pm10, tvoc, co2, temperature, humidity, illumination, noise, hcho, co,c6h6, no2, o3) VALUES "
    query_value_str = ""
    
    # every 500 rows, insert once to DB
    input_file = csv.DictReader(open("./data_storage/" + file_name))
    for row in input_file:
        if row_count > 0:
            query_value_str = query_value_str + "(" + "'" + row["location"] +  "'" + "," + "'"  + row["timestamp"] + "'" + "," + row["pm2_5"] + "," +  row["pm10"] + "," +  row["tvoc"] + "," + row["co2"] + "," + row["temperature"] + "," + row["humidity"] + "," + row["illumination"] + "," + row["noise"] + "," + row["hcho"]+ "," + row["co"] + "," + row["c6h6"] + "," + row["no2"] + "," + row["o3"]+ ")" + ","
            row_count = row_count - 1
        else:
            query_value_str = ""
            row_count = 100
            query_value_str = query_value_str[0: len(query_value_str) -1]
            query_insert_statement = query_insert_str + query_value_str
            print (query_insert_statement)
            cur.execute(query_insert_statement)
            conn.commit()
    
    query_value_str = query_value_str[0: len(query_value_str) -1 ]
    query_insert_statement = query_insert_str + query_value_str
    print (query_insert_statement)
    cur.execute(query_insert_statement)
    conn.commit()
    
    
    cur.close()
    conn.close()
        
#store_2_remote_db()

# FTP to remote server file folder
def ftp_2_remote_file_folder(file_name):

    print ("The file " + file_name + " is uploading")
    local_migration_file = "./data_storage/" +file_name
    ftp_server = "138.100.82.181"
    username = "sun"
    password = "Madrid2018"
    ftp = FTP(ftp_server)
    ftp.login(username, password)
    print ("FTP connection built " + ftp.welcome)

    file = open(local_migration_file, "rb")
    ftp.cwd(remote_ftp_dir)

    ftp.storbinary('STOR file_received.csv', file)
    ftp.quit()
    file.close()


# email sending
def sent_file_as_email(file_name):
    ts = time.time()
    dt = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M")
    
    msg = MIMEMultipart()
    msg["From"] = "sunshengjing@gmail.com"
    msg["To"] = COMMASPACE.join("sunshengjing@gmail.com")
    msg["Subject"] = "new iaq data " + dt

    msg.attach(MIMEText("Check " + file_name + " Thanks ! "  ))
    part = MIMEBase("application", "octet-stream")
    with open("./data_storage/" +file_name) as file:
        part.set_payload(file.read())
    encoders.encode_base64(part)
    part.add_header("Content-Disposition", 'attachment; filename = "file_received.csv"')
    msg.attach(part)
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login("sunshengjing@gmail.com", "sun1988SSJ")
    server.sendmail("sunshengjing@gmail.com", "sunshengjing@gmail.com", msg.as_string())
    print ("email sent !")
    server.close()


# entrance

remote_ftp_dir = "/home/sun/IAQ_SUN/FTP/data_ftp_receiving/"

ip = "192.168.10.102"
port = 13
sock = socket.socket (socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((ip, port))

csv_file = create_new_file("/home/pi/Desktop/data_storage/data_mushroom_", 0)
print ("file " + "data_mushroom_" + str (0) + " is created")
fieldnames = ['location', 'timestamp', 'pm2_5', 'pm10', 'tvoc', 'co2', 'temperature', 'humidity', 'illumination', 'noise', 'hcho', 'co', 
'c6h6', 'no2', 'o3']
writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
writer.writeheader()
print ("file header is added")

# counting 
file_row_count = 10
file_count = 0

while True:

    ts = time.time()
    dt = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M")
    
    data_1_min_list = data_1_min_collect(60, sock)
    data_dict = parse_data_1_min_list(data_1_min_list)
    data_dict["timestamp"] = dt
    data_dict["location"] = "Celsa Group"
    print ("one row is added")
    print (data_dict)
    writer.writerow(data_dict)
    file_row_count = file_row_count - 1
    if (file_row_count < 0):
        csv_file.close()
        
        #store file data directly to remote server DB
        store_2_remote_db("data_mushroom_" + str(file_count) + ".csv")
        
        # FTP to remote server folder
        #ftp_2_remote_file_folder("data_mushroom_" + str(file_count) + ".csv")
        
        # Email file to my gmail mailbox
        #sent_file_as_email("data_mushroom_" + str(file_count) + ".csv")
        
        # create a new file
    	file_count = file_count + 1
    	file_row_count = 10
    	csv_file = create_new_file("./data_storage/data_mushroom_", file_count)
    	print ("file " + "data_mushroom_" + str (file_count) + " is created")
    	writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    	writer.writeheader()
    	print ("file header is added")
    	





