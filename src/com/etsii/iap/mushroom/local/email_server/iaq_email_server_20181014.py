#!/usr/bin/python

""" shengjing  """
""" version 0.1 """
""" date 2018-10-11 """
""" server end to parse email received file to DB"""



import socket
import datetime
import time
import csv
import pymysql
import os
import shutil
import email, getpass, imaplib

from ftplib import FTP

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from email.utils import COMMASPACE


""" check file folder, found the received file """

def locate_new_received_file(received_folder_dir):

    file_list = os.listdir(received_folder_dir) # dir is your directory path
    number_files = len(file_list)
    print (number_files)
    return number_files

"""DATA file to Remote server"""

# insert directly to remote DB
def store_2_remote_db(file_dir):
    conn = pymysql.connect(host = "138.100.82.177", port = 3306, user = "root", passwd = "owntracks", db = "mushroom")
    cur = conn.cursor()
    row_count = 100
    
    query_insert_str = "insert into mushroom_measurement (location, timestamp, pm2_5, pm10, tvoc, co2, temperature, humidity, illumination, noise, hcho, co,c6h6, no2, o3) VALUES "
    query_value_str = ""
    
    # every 500 rows, insert once to DB
    input_file = csv.DictReader(open(file_dir))
    for row in input_file:
        if row_count > 0:
            query_value_str = query_value_str + "(" + "'" + row["location"] +  "'" + "," + "'"  + row["timestamp"] + "'" + "," + row["pm2_5"] + "," +  row["pm10"] + "," +  row["tvoc"] + "," + row["co2"] + "," + row["temperature"] + "," + row["humidity"] + "," + row["illumination"] + "," + row["noise"] + "," + row["hcho"]+ "," + row["co"] + "," + row["c6h6"] + "," + row["no2"] + "," + row["o3"]+ ")" + ","
            row_count = row_count - 1
        else:
            query_value_str = ""
            row_count = 100
            query_value_str = query_value_str[0: len(query_value_str) -1]
            query_insert_statement = query_insert_str + query_value_str
            print (query_insert_statement)
            cur.execute(query_insert_statement)
            conn.commit()
    
    query_value_str = query_value_str[0: len(query_value_str) -1 ]
    query_insert_statement = query_insert_str + query_value_str
    print (query_insert_statement)
    cur.execute(query_insert_statement)
    conn.commit()

    cur.close()
    conn.close()
        

def clear_file_folder(dir):
# delete files not folder itself
    for root, dirs, files in os.walk(dir):
        for f in files:
            os.unlink(os.path.join(root, f))
    for d in dirs:
        shutil.rmtree(os.path.join(root, d))


def check_new_email(received_folder_dir):

    user = 'iaq.etsii@gmail.com'
    pwd = 'mushroom_1324'

    # connecting to the gmail imap server
    mail = imaplib.IMAP4_SSL("imap.gmail.com")
    mail.login(user,pwd)
    mail.select('inbox') # here you a can choose a mail box like INBOX instead
    type, items = mail.search(None, 'UNSEEN')
    for email_id in items[0].split()[::-1]:
        resp, data = mail.fetch(email_id, "(RFC822)") # fetching the mail, "`(RFC822)`" means "get the whole stuff", but you can ask for headers only, etc
        email_body = data[0][1] # getting the mail content
        message = email.message_from_bytes(email_body) # parsing the mail content to get a mail object

        if "new iaq data" not in message["Subject"]:
            continue
        #Check if any attachments at all
        print ("Found a new email")
        if message.get_content_maintype() != 'multipart':
            continue

        print ("["+message["From"]+"] :" + message["Subject"])

        for part in message.walk():
            if part.get_content_maintype() != "multipart" and part.get("Content-Disposition") is not None:
                f = open(received_folder_dir + part.get_filename(), "wb")
                f.write(part.get_payload(decode = True))
                f.close()


""" Entrance"""

# download email attachment to dir
received_folder_dir = "/Users/shengjing/Desktop/data_email_receiving/"

while True:

    # checking new emails and download email attachment to dir
    print ("checking new email")
    check_new_email(received_folder_dir)

    file_number = locate_new_received_file(received_folder_dir)
    if file_number == 1:
        print ("Found a downloaded attachment file")
        print ("Store to DB")
        store_2_remote_db("/Users/shengjing/Desktop/data_email_receiving/file_received.csv")
        # delete the new received file
        print ("delete the file")
        clear_file_folder(received_folder_dir)
        # wait for 2 mins
    time.sleep(120)






