package com.etsii.iap.mushroom;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.circulate.rt.CirculatertsAbs;
import org.circulate.rt.ResultAirData;
import org.circulate.rt.ResultObject;
import org.circulate.rt.em.BusType;
import org.circulate.rt.util.CulField;

/**
 * Author: alex, shengjing Email: 2850317192@qq.com Date: 2017/11/09
 */
public class Circulatert extends CirculatertsAbs {

	/**
	 * Gateway
	 */
	static final String GATEWAY = "http://www.huanjingkong.com/airdatadev/";

	/**
	 * developer Id
	 */
	String developerId = "338054848";

	/**
	 * developer secret
	 */
	String secret = "31752132";

	/**
	 * email
	 */
	String email = "";

	/**
	 * input chart set
	 */
	String inputCharset = "UTF-8";

	/**
	 * lang(zh,en)
	 */
	String nationalLanguage = "en";

	/**
	 * bus type
	 */
	String busType = BusType.GATEDATE.name();

	/**
	 * APP pri key
	 */
	String appPriKey = "";

	/**
	 * APP pub key
	 */
	String appPubKey = "";

	/**
	 * config
	 */
	Map<String, String> culConfig;

	public Circulatert(String developerId, String secret, String email, String inputCharset, String busType,
			String appPriKey, String appPubKey, Map<String, String> culConfig) {
		super();
		this.developerId = developerId;
		this.secret = secret;
		this.email = email;
		this.inputCharset = inputCharset;
		this.busType = busType;
		this.appPriKey = appPriKey;
		this.appPubKey = appPubKey;
		this.culConfig = culConfig;
	}

	public Circulatert() {
		super();
		initConfig();
		// TODO Auto-generated constructor stub
	}

	private void initConfig() {
		culConfig = new HashMap<String, String>();
		culConfig.put(CulField.developerId, developerId);
		culConfig.put(CulField.secret, secret);
		culConfig.put(CulField.email, email);
		culConfig.put(CulField.busType, busType);
		culConfig.put(CulField.GATEWAY, GATEWAY);
		culConfig.put(CulField.nationalLanguage, nationalLanguage);
	}

	public ResultAirData GATEAIRDATA(ResultObject obj) {
		return super.GATEAIRDATA(obj);
	}

	public ResultObject sendRequest(String deviceId, String userId) {
		this.culConfig.put(CulField.deviceId, deviceId);
		this.culConfig.put(CulField.userId, userId);
		return sendNetRequest(this.culConfig);
	}

	public boolean verify(ResultObject obj) {
		return super.verify(obj);
	}

	public static void main(String[] args) {
		try {
			File file = new File("mushroom_readings.csv");
			file.createNewFile();
			FileWriter fileWriter = new FileWriter(file);
			// headers start
			// String resultAirData = readCirculatert("WNYRCI0", "338054848");
			String resultAirData = readCirculatert("WJJHV00", "338054848");
			String fileHeaders = "";
			// add time stamp
			fileHeaders = fileHeaders.concat("Timestamp,");
			// add the rest of air sensor fields
			fileHeaders = fileHeaders + getFileHeaders(resultAirData);
			System.out.println("The file header are: " + fileHeaders);
			fileWriter.write(fileHeaders);
			fileWriter.write("\n");
			fileWriter.flush();
			// headers end
			// data measurement starts
			int countFileLine = 2000;
			while (true) {
				// read data from broad
				// String resultAirData1 = readCirculatert("WNYRCI0", "338054848");
				String resultAirData1 = readCirculatert("WJJHV00", "338054848");
				save2File(resultAirData1, fileWriter);
				// check line count in file, if grater than 2000, create a new file
				countFileLine--;
				if (countFileLine <= 0) {
					File f = createFile("mushroom_readings.csv");
					fileWriter = new FileWriter(f);
					// add headers start
					fileWriter.write(fileHeaders);
					fileWriter.write("\n");
					fileWriter.flush();
					// add headers end
					countFileLine = 2000;

				}
				// read data from broad 2
				// String resultAirData2 = readCirculatert("WJJHV00", "338054848");
				Thread.sleep(1000 * 60);
			}
		} catch (Exception e) {
			System.out.println("Exception !! " + e.getMessage());
		}

	}

	private static File createFile(String name) {
		File f;
		int count = 0;
		f = new File(name);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (Exception e) {
				System.out.println("Exception !! " + e.getMessage());
			}
		} else {
			count++;
			createFile(name + (count));
		}
		return f;
	}

	private static String readCirculatert(String deviceId, String userId) {
		Circulatert circulatert = new Circulatert();
		ResultObject object = circulatert.sendRequest(deviceId, userId);
		// System.out.println(object.getResultData().toString());
		if (circulatert.verify(object)) {
			ResultAirData resultAirData = circulatert.GATEAIRDATA(object);
			// System.out.println(resultAirData.toString());
			return resultAirData.toString();
		} else {
			return null;
		}
	}

	private static String getFileHeaders(String resultAirData) {
		String fileHeaders = "";
		Pattern p = Pattern.compile("AirIndexData.*?]");
		Matcher m = p.matcher(resultAirData);
		while (m.find()) {
			// get headers
			String matcher = m.group();
			String header = matcher.split(",")[1].split("=")[1];
			fileHeaders = fileHeaders.concat(header).concat(",");
			// System.out.println(m.group());

		}
		// delete the last , in headers
		fileHeaders = fileHeaders.substring(0, fileHeaders.length() - 1);
		return fileHeaders;
	}

	private static void save2File(String resultAirData, FileWriter fileWriter) {
		String dataString = "";
		// get timestamp
		DateFormat dataFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String timeStamp = dataFormat.format(cal.getTime());
		dataString = timeStamp + "," + dataString;
		int hasData = 2;
		try {
			Pattern p = Pattern.compile("AirIndexData.*?]");
			Matcher m = p.matcher(resultAirData);
			while (m.find()) {
				// get headers
				String matcher = m.group();
				String data = matcher.split(",")[0];
				hasData = data.split("=").length;
				if (hasData == 2) {
					dataString = dataString.concat(data.split("=")[1]).concat(",");
				} else {
					dataString = dataString.concat("0").concat(",");
				}
			}
			// delete the last , in data string
			dataString = dataString.substring(0, dataString.length() - 1);
			System.out.println("The storing data line is : " + dataString);

			fileWriter.write(dataString);
			fileWriter.write("\n");
			fileWriter.flush();
			// fileWriter.write("\n");
		} catch (Exception e) {
			System.out.println("Exception ! " + e.getMessage());
		}

	}

}